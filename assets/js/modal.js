const modalLogin = document.getElementById("modal_login")
let isShow = false
document.addEventListener("click", () => {
  isShow = !isShow
  if(isShow) {
    modalLogin.classList.add("active")
    return
  }
  modalLogin.classList.remove("active")
})
modalLogin.querySelector(".modal-dialog").addEventListener("click", (e) => {
  e.stopPropagation()
})